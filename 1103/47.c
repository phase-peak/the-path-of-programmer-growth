main()
{ int i,num1,num2,temp;
   printf("The fraction series with demominator 40 is :\n");
   for(i=1;i<=40;i++){
      num1=40;
      num2=i;
      while(num2!=0){
        temp=num1%num2;
        num1=num2;
        num2=temp;
      }
      if(num1==1)
      printf("%d/40\n",i);
   }

}