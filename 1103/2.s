	.file	"2.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	.LC0(%rip), %xmm0
	movsd	%xmm0, -8(%rbp)
	jmp	.L2
.L7:
	movq	-8(%rbp), %rax
	movq	%rax, %xmm0
	call	acos@PLT
	movsd	.LC1(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %eax
	movl	%eax, -12(%rbp)
	movl	$1, -16(%rbp)
	jmp	.L3
.L4:
	movl	$32, %edi
	call	putchar@PLT
	addl	$1, -16(%rbp)
.L3:
	movl	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jl	.L4
	movl	$32, %edi
	call	putchar@PLT
	jmp	.L5
.L6:
	movl	$32, %edi
	call	putchar@PLT
	addl	$1, -16(%rbp)
.L5:
	movl	$62, %eax
	subl	-12(%rbp), %eax
	cmpl	%eax, -16(%rbp)
	jl	.L6
	movsd	-8(%rbp), %xmm0
	movsd	.LC2(%rip), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rbp)
.L2:
	movsd	-8(%rbp), %xmm0
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L7
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.align 8
.LC1:
	.long	0
	.long	1076101120
	.align 8
.LC2:
	.long	-1717986918
	.long	1069128089
	.align 8
.LC3:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	1f - 0f
	.long	4f - 1f
	.long	5
0:
	.string	"GNU"
1:
	.align 8
	.long	0xc0000002
	.long	3f - 2f
2:
	.long	0x3
3:
	.align 8
4:
