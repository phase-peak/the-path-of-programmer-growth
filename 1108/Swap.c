#include<stdio.h>

void Swap(int *a,int *b)
{
    int temp=*a;
    *a=*b;
    *b=temp;
                           //printf("Swap: a: %d b: %d\n",a,b);
}

int main()
{
    int a=20;
    int b=10;
    Swap(&a,&b);
    printf("Main: a: %d b: %d\n",a,b);
                                            //int temp=a;
                                            //a=b;
                                            //b=temp;
                                            //printf("a: %d b: %d\n",a,b);
    return 0;
}
