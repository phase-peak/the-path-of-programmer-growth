#include <stdio.h>

int main()
{
    const int a=10;
    int* const p=&a;
    *p=20;
    int b=20;
    p=&b;
    printf("a: %d\n",a);
    return 0;
}